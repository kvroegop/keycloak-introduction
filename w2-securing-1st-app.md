# Securing your first application

Before beginning with this task, you first need to clone git repo form the url: https://bitbucket.org/first8/keycloak/src/Master/

This git repo contains a backend and frontend application which is the starting point for this assignment. The two applications are not yet secured so it is your task to connect keycloak with both application to create a secure experience.

## Configure keycloak

In the side menu click on Clients.
Click on the button "Create" in the right corner of the table.
Fill in the Client ID: backend
Click on save

Fill in the columns: 
* Access Type: bearer-only.

After this click on save.

Click on Credentials in the top bar and Regenerate a Secret. copy this secret and copy-paste it in the application.properties (backend\src\main\resources\application.properties) under the property: keycloak.credentials.secret.

Go back to Clients
Click on the button "Create" in the right corner of the table.
Fill in the Client ID: frontend
Click on save
Fill in the columns: 
* Access Type: public 
* valid redirect Uris: * 
* web Origins: *

Next go to click on users in the side menu. click on add user and fill in the following values:

* username: aap

Click on save. After this click on credentials in de top menu and fill in the following values:

* Password: first8
* Password confirmation: first8
* Temporary: off

Click on Reset Password.



# Integrate frontend with keycloak

Uncomment the code inside the `main.ts` (frontend\src\main.ts). This step uses the keycloak adapter to connect the frontend application with the keycloak server. first we import de keycloak adapter second we create the keycloakconfig object to let keycloak-adapter know which url, realm and clientId it needs to use. Third we check initialize the keycloak adapter and check if the user is already logged in. last but not least we implement a loop to refresh the token when it expires.

Next go to `Navbar.vue` (frontend\src\components\Navbar.vue) and uncomment the code of all the 'keycloak task 2' occurrences. This is so the user has a button to log in and log out from keycloak.

# Integrate backend with keycloak

Uncomment the keycloak dependencies inside the pom.xml file (backend\pom.xml)

Uncomment the keycloak properties inside the application.properties file (backend\src\main\resources\application.properties)

Uncomment all code inside `KeycloakConfig.java` (backend\src\main\java\com\workshop\keycloak\backend\config\KeycloakConfig.java) this config file lets the backend server know how to properly connect with the keycloak server.

Comment out all code inside `SpringConfig.java` (backend\src\main\java\com\workshop\keycloak\backend\config\SpringConfig.java) this was only for temporary use.


# Testing

After all these changes in the frontend, is the base code and the production code out of sync therefore we need to run the command: `mvn package` in the root folder of the project. This command packages the vue files and places them inside the proper folder in the backend so the backend can serve the new production ready frontend.
